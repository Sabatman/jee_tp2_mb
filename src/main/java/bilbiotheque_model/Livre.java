package bilbiotheque_model;

public class Livre {

	private String titre;
	private int annee;
	private String editeur;
	private String nomAuteur;
	private String prennomAuteur;
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public String getEditeur() {
		return editeur;
	}

	public void setEditeur(String editeur) {
		this.editeur = editeur;
	}

	public String getNomAuteur() {
		return nomAuteur;
	}

	public void setNomAuteur(String nomAuteur) {
		this.nomAuteur = nomAuteur;
	}

	public String getPrennomAuteur() {
		return prennomAuteur;
	}

	public void setPrennomAuteur(String prennomAuteur) {
		this.prennomAuteur = prennomAuteur;
	}
	public Livre() {
		setPrennomAuteur(null);
		setNomAuteur(null);
		setEditeur(null);
		setAnnee(-1);
		setTitre(null);
		setId(0);
	}
	public Livre(String pa, String na, String edit, int  an, String tittle, int id) {
		setPrennomAuteur(pa);
		setNomAuteur(na);
		setEditeur(edit);
		setAnnee(an);
		setTitre(tittle);
		setId(id);
	}
}
