package bilbiotheque_controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class EmpruntsDAO {

	private java.sql.Connection connect;
	
	public EmpruntsDAO() throws SQLException {
		// 1 - faire connection DB
		connect = DriverManager.getConnection("jdbc:sqlite:bibliotheque.sqlite"); // C'est une methode statique (une facrory
																				// = qui creer des objets)
		// "jdbc:mysql://localhost/base1"
		// 2 - requete DB
		Statement st = connect.createStatement(); // creation de la requete (en anglais on peut dire statement)
		
	}
	
	public ArrayList<ArrayList<String>> selectTableEmprunts() throws SQLException {

		
		PreparedStatement selectEmp = connect.prepareStatement("SELECT titre, nom, date_emprunt \r\n"
				+ "FROM Livres, Emprunts, Usager \r\n" + "WHERE Livres.id_livre = Emprunts.id_livre \r\n"
				+ "AND Emprunts.id_usager = Usager.id_usager\r\n" + "Order by date_emprunt");
		ResultSet rs = selectEmp.executeQuery();
		// coucou
		ArrayList<ArrayList<String>> lTableEmprunt = new ArrayList<ArrayList<String>>();
		while (rs.next()) {
			System.out.println(String.format("Titre: %s \t nom: %12s \t date_emprunt: %s", rs.getString("titre"),
					rs.getString("nom"), rs.getString("date_emprunt")));
			ArrayList<String> lUneLigneEmprunt = new ArrayList<String>();
			lUneLigneEmprunt.add(rs.getString("titre"));
			lUneLigneEmprunt.add(rs.getString("nom"));
			lUneLigneEmprunt.add(rs.getString("date_emprunt"));
			lTableEmprunt.add(lUneLigneEmprunt);
		}
		return lTableEmprunt;
	}

	public void creerEmprunt(String livre, String usager, LocalDateTime dateEmprunt) {
		
		// ici verifier si livre existe et recupérer id
		try {
			
			int idLivre = getIdLivre(livre);

			// creation de usager et recuperer son id
			//
			int idUsager = selectIdUsager(usager);
			if (idUsager > -1 && idLivre > -1) {
			
				// creation de l'emprunt
	
				PreparedStatement preStateInsertEmprunt = connect.prepareStatement(
						"INSERT INTO Emprunts (date_emprunt, date_rendu, id_usager, id_livre) VALUES (?,?,?,?)");
				preStateInsertEmprunt.setString(1, dateEmprunt.toString());
				preStateInsertEmprunt.setString(2, "non rendu");
				preStateInsertEmprunt.setInt(3, idUsager);
				preStateInsertEmprunt.setInt(4, idLivre);
				preStateInsertEmprunt.executeUpdate();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

	}

	public int getIdLivre(String livre) throws SQLException {
		
		PreparedStatement preStateSelectLivre = connect.prepareStatement("SELECT * FROM Livres WHERE titre = ?");
		preStateSelectLivre.setString(1, livre);
		ResultSet resultSetLivre = preStateSelectLivre.executeQuery();
		int compteur = 0;
		int idLivre = -1;
		while (resultSetLivre.next()) {
			compteur++;

		}
		if (compteur > 0) {

			resultSetLivre = preStateSelectLivre.executeQuery();
			while (resultSetLivre.next()) {
				idLivre = resultSetLivre.getInt(1);
				if (checkIdLivreEmprunt(idLivre) == false) {
					System.out.println("Livre disponible pour emprunt");
					return idLivre;
				} else {
					System.out.println("Livre non-disponible pour emprunt");
					idLivre = -1;
				}
			}
		}
		return idLivre;
	}

	public boolean checkIdLivreEmprunt(int idLivre) {
		boolean checkEmprunt = false;
		try {
			PreparedStatement preStateSelectCheck = connect
					.prepareStatement("SELECT * FROM Emprunts WHERE id_livre = ?");
			preStateSelectCheck.setInt(1, idLivre);
			ResultSet resultSetCheck = preStateSelectCheck.executeQuery();
			checkEmprunt = resultSetCheck.next();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return checkEmprunt;
	}

	public int selectIdUsager(String nomU) {

		try {
			
			PreparedStatement selectUsa = connect.prepareStatement("SELECT id_usager From Usager Where nom = ?");
			selectUsa.setString(1, nomU);
			ResultSet rs = selectUsa.executeQuery();
			return rs.getInt("id_usager");
			// test
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return -1;
	}

}
