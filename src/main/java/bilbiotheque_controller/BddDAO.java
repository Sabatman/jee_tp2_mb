package bilbiotheque_controller;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.sql.DataSource;


import bilbiotheque_model.Livre;

public class BddDAO {
	
	private static Connection connec;

	public BddDAO () throws SQLException {
		// parametres

		String url = "jdbc:sqlite:bibliotheque.sqlite";
		connec = DriverManager.getConnection(url);
			
		Statement st = connec.createStatement();
		st.executeUpdate("CREATE TABLE IF NOT EXISTS  Livres " + "(id_livre INTEGER PRIMARY KEY,"
				+ " titre TEXT NOT NULL," + " annee INT NOT NULL," + " editeur TEXT NOT NULL,"
				+ "nom_auteur TEXT NOT NULL, " + "prenom_auteur TEXT NOT NULL); "
				+ "CREATE TABLE IF NOT EXISTS Usager (id_usager INTEGER PRIMARY KEY,"
				+ " nom TEXT NOT NULL);"
				+ "CREATE TABLE IF NOT EXISTS Emprunts (id_emprunts INTEGER PRIMARY KEY, date_emprunt TEXT, date_rendu TEXT,"
				+ " id_usager INTEGER, id_livre INTEGER, FOREIGN KEY(id_usager) REFERENCES Usager(id_usager), FOREIGN KEY(id_livre) REFERENCES Livres(id_livre));");
		// création de trois tables différentes : livres, usagers et emprunts
		System.out.println("Connection à la base de donnée réussie");
	}


	// Recuperation de tous les livres dans la BDD
	
	public List<Livre> getLivres() throws SQLException{
		
		List<Livre> listl = new ArrayList<>();
		String titre;
		int annee;
		String editeur;
		String nom;
		String prenom;
		int id;
		
		PreparedStatement tl = connec.prepareStatement("SELECT * FROM Livres");
		ResultSet resultSetLivre = tl.executeQuery();

		while (resultSetLivre.next()) {
			Livre tempoBook;
			titre =resultSetLivre.getString("titre");
			annee =resultSetLivre.getInt("annee");
			editeur =resultSetLivre.getString("editeur");
			nom =resultSetLivre.getString("nom_auteur");
			prenom =resultSetLivre.getString("prenom_auteur");
			id =resultSetLivre.getInt("id_livre");
			tempoBook = new Livre(prenom,nom,editeur,annee,titre,id);
			listl.add(tempoBook);	
		}
		return listl;
	}
		
}
