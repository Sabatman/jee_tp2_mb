package bibliotheque_main;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import bilbiotheque_controller.BddDAO;
import bilbiotheque_model.Livre;


@RestController
public class External_controller {
	
	@GetMapping(value="/biblio", produces ="application/json")
	public List<Livre> getLivres() throws SQLException{
		List<Livre> ll = new ArrayList<>();
		BddDAO conn = new BddDAO();
		return ll = conn.getLivres();
	}
	
	@GetMapping(value="/biblio/{id}", produces="application/json")
	public Livre getLivre(@PathVariable("id") int n) throws SQLException {
		System.out.println("coucou");
		return getLivres().get(n);
	}
}
