package bibliotheque_main;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import bilbiotheque_model.Livre;

public class BiblioClient {

	public static void main(String[] args) {
		try {
			RestTemplate rt = new RestTemplate();
			Livre recipe2 = rt.getForObject("http://localhost:8080/biblio/1", Livre.class);
			System.out.println("Show me a recipe: " + recipe2);
			
			ResponseEntity<List<Livre>> response = rt.exchange("http://localhost:8080/biblio", 
					HttpMethod.GET,null, new ParameterizedTypeReference<List<Livre>>(){});
			List<Livre> employees = response.getBody();
			
			//List<Livre> plouf = (rt.getForObject("http://localhost:8080/biblio", Livre.class);
			System.out.println("bonjoooouuuuuuuuuuuuuuuuuuuuuuuur" + employees);


		} catch(Exception ex) { ex.printStackTrace();}
	}

}
